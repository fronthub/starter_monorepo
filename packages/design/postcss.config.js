const postCSSConfig = require('../../config/postcss.config');

module.exports = {
  ...postCSSConfig
};
