/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
const jestConfig = require('../../config/jest.config');

module.exports = {
  ...jestConfig
};
