// Essayer de concevoir une interface complète de déclaration des props standardisé pour pouvoir alimenter 2 choses:
// - le format Framework : Vue, React...
// - le format Documentation
// - la gestion ou non de typescript

// C'est à dire que l'on a une sorte de lib qui gère les props et leur validation que d'une seule manière
// et elle est ensuite implémentée de façon différente en fonction des languages et de typescript

// @ts-nocheck
const useDocsProps: Object = (props) => {
  const propsObject = {}
  Object.entries(props).forEach(([key, values]) => {
    let newValues = {}
    if(Array.isArray(values.type)){
      newValues = {
        ...values,
        type: String,
        doc: {
          control: { type: 'select' },
          options: values.type
        },
        validator(value) {
          return values.type.includes(value);
        },
      }
    }else{
      const type = values.type === String ? 'string' : values.type === Boolean ? 'boolean' : ''
      // console.log('values.type', values.type, type)
      newValues = {
        ...values,
        doc: {
          control: { type: type }
        },
      }
    }
    propsObject[key] = newValues
  })
  // console.log('propsObject', propsObject)
  return propsObject
};

export default useDocsProps;
