module.exports = {
  title: 'IAD Design System',
  description: 'Just playing around.',
  head: [
    ['link', { rel: 'preconnect', href: 'https://fonts.googleapis.com' }],
    ['link', { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: true }],
    ['link', { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600&display=swap' }]
  ],
  themeConfig: {
    nav: [
      { text: "Home", link: "/" },
      { text: "Foo", link: "/foo/" },
      { text: "Bar", link: "/bar/" }
    ],
    sidebar: [
      {
        title: 'Accueil',
        path: '/',
        collapsable: true,
        sidebarDepth: 1,
        children: [
          { text: "Accueil", link: "/" },
          { text: "Avatar", link: "/composants/avatar" },
          { text: "Button", link: "/composants/button" },
          { text: "Icon", link: "/composants/icon" },
        ]
      },
    ],
  },
}
