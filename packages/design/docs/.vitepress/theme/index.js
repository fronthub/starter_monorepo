import DefaultTheme from 'vitepress/theme'
import PlayGround from '../components/PlayGround.vue'
import PreView from '../components/PreView.vue'
import Variants from '../components/Variants.vue'
import './index.css';

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.component('PlayGround', PlayGround)
    app.component('PreView', PreView)
    app.component('Variants', Variants)
  }
}
