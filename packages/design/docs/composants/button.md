<script setup>
import Button from '../../src/components/button/Button.vue'
</script>

# Button

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus. Sed feugiat luctus placerat. Curabitur luctus semper venenatis. Maecenas lobortis facilisis arcu a sollicitudin. Etiam sit amet ante nec felis gravida egestas. Praesent commodo dolor sem, vitae iaculis nisi pellentesque ut. Nunc luctus finibus ex eu egestas. Maecenas ante nibh, imperdiet in sodales non, ornare ac sem.

[[toc]]

## Playground

<PlayGround :component="Button" :args="{
  label: 'button',
}" />

## Types

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<Variants :component="Button" variant="type" :args="{
  label: 'button',
}" />

## Position Icon

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<PreView>
<Button label="button" />
<Button label="button" icon="absence" />
<Button label="button" icon="absence" iconRight />
</PreView>

## Icon seul

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<PreView class="bg-primary">
<Button icon="absence" />
</PreView>

## Trucs en vrac

## Source Code

<<< @/../src/components/button/Button.vue

## Docs Code

<<< @/../docs/composants/button.md

<!-- ## Tests Code

<<< @/../tests/Avatar.spec.ts -->

## Style Code

<<< @/../src/components/button/Button.pcss
