<script setup>
import Icon from '../../src/components/icon/Icon.vue'
</script>

# Icon :tada: :100:

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus. Sed feugiat luctus placerat. Curabitur luctus semper venenatis. Maecenas lobortis facilisis arcu a sollicitudin. Etiam sit amet ante nec felis gravida egestas. Praesent commodo dolor sem, vitae iaculis nisi pellentesque ut. Nunc luctus finibus ex eu egestas. Maecenas ante nibh, imperdiet in sodales non, ornare ac sem.

[[toc]]

## Playground

<PlayGround :component="Icon" :args="{
  iconName: 'archive',
}" />

## Sizes

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<Variants :component="Icon" variant="size" :args="{
  iconName: 'archive',
}" />

## Colors

<PreView class="text-pink">
<Icon icon-name="archive" />
<Icon icon-name="archive" current-color class="text-teal" />
<Icon icon-name="archive" current-color />
</PreView>

## Source Code

<<< @/../src/components/icon/Icon.vue

## Docs Code

<<< @/../docs/composants/icon.md

<!-- ## Tests Code

<<< @/../tests/Avatar.spec.ts -->

## Style Code

<<< @/../src/components/icon/Icon.pcss
