<script setup>
import Avatar from '../../src/components/avatar/Avatar.vue'
</script>

# Avatar :tada: :100:

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus. Sed feugiat luctus placerat. Curabitur luctus semper venenatis. Maecenas lobortis facilisis arcu a sollicitudin. Etiam sit amet ante nec felis gravida egestas. Praesent commodo dolor sem, vitae iaculis nisi pellentesque ut. Nunc luctus finibus ex eu egestas. Maecenas ante nibh, imperdiet in sodales non, ornare ac sem.

[[toc]]

## Playground

<PlayGround :component="Avatar" />

## Sizes

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<Variants :component="Avatar" variant="size" />


## Colors

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<Variants :component="Avatar" variant="color" />

## With Image

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eu convallis risus.

<PreView>
  <Avatar />
  <Avatar initials="SA"/>
  <Avatar urlPicture="https://cdn.quasar.dev/img/avatar.png" />
  <Avatar initials="SA" urlPicture="https://cdn.quasar.dev/img/avatar.png" />
</PreView>

## Source Code

<<< @/../src/components/avatar/Avatar.vue

## Docs Code

<<< @/../docs/composants/avatar.md

## Tests Code

<<< @/../tests/Avatar.spec.ts

## Style Code

<<< @/../src/components/avatar/Avatar.pcss
