// eslint-disable-next-line import/no-extraneous-dependencies
import { shallowMount } from '@vue/test-utils';
import Avatar from '@/components/avatar/Avatar.vue';

describe('Avatar.vue', () => {
  it('renders props.initials when passed', () => {
    const initials = 'SA';

    const wrapper = shallowMount(Avatar, {
      props: { initials }
    });
    expect(wrapper.text()).toMatch(initials);
    expect(true).toBe(true);
  });
});
