const eslintConfig = require('../../config/.eslintrc');

module.exports = {
  ...eslintConfig
};
