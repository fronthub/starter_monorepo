export default [
  'primary',
  'secondary',
  'brown',
  'green',
  'grey',
  'greyblue',
  'pink',
  'purple',
  'teal',
  'yellow',
  'red',
  'amber'
];
