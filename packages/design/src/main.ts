// eslint-disable-next-line import/no-extraneous-dependencies
import { createApp } from 'vue';
import App from '@/App.vue';
import '@/assets/css/tailwind.css';

createApp(App).mount('#app');
