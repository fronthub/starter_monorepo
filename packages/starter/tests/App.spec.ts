// eslint-disable-next-line import/no-extraneous-dependencies
import { mount } from '@vue/test-utils';
import App from '@/App.vue';

describe('App.vue', () => {
  it('renders DS component', () => {
    const initials = 'SA';

    const wrapper = mount(App);
    expect(wrapper.text()).toMatch(initials);
    expect(true).toBe(true);
  });
});
