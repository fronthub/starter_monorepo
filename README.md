# IAD Vue3 MonoRepo

Repo Git : [https://bitbucket.org/fronthub/starter_monorepo/admin](https://bitbucket.org/fronthub/starter_monorepo/admin)

DS Preview : [https://starter-monorepo-ds.vercel.app/composants/avatar.html](https://starter-monorepo-ds.vercel.app/composants/avatar.html)

App Preview : [https://starter-monorepo-app.vercel.app](https://starter-monorepo-app.vercel.app)

### Ressources

Workspace
https://medium.com/@jsilvax/a-workflow-guide-for-lerna-with-yarn-workspaces-60f97481149d
https://www.linkedin.com/pulse/things-i-wish-had-known-when-started-javascript-monorepo-gorej/?

Config Jest
https://dev.to/imomaliev/creating-vite-vue-ts-template-setup-jest-5h1i
