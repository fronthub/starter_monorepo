module.exports = {
  env: {
    browser: true,
    es2021: true,
    'vue/setup-compiler-macros': true, // fix for error  'defineProps' is not defined no-undef
    'jest/globals': true
  },
  extends: [
    'eslint:recommended',
    'airbnb-base',
    'plugin:import/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:jest/recommended'
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    parser: '@typescript-eslint/parser',
    sourceType: 'module'
  },
  plugins: [
    'vue',
    'jest',
    '@typescript-eslint'
  ],
  rules: {
    'comma-dangle': ['error', 'never'],
    'newline-before-return': 'error',
    'template-curly-spacing': ['error', 'always'],
    'import/extensions': [
      'off'
    ],
    'import/no-unresolved': 'off', // pk ?
    'import/no-extraneous-dependencies': [
      'off'
    ]
  },
  ignorePatterns: ['dist/', 'node_modules/']
};
