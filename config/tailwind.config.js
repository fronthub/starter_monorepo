const plugin = require('tailwindcss/plugin');
const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  content: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx}',
    './docs/**/*.md'
  ],
  theme: {
    fontSize: {
      xs: ['10px', '13px'],
      sm: ['11px', '16px'],
      base: ['14px', '19px'],
      lg: ['16px', '22px'],
      xl: ['20px', '27px'],
      '2xl': ['34px', '46px']
    },
    fontWeight: {
      light: 300,
      normal: 400,
      bold: 600
    },
    letterSpacing: {
      normal: '0',
      tightest: '.0014px',
      tight: '.0018px',
      wide: '.0085px',
      widest: '.0175px'
    },
    colors: {
      transparent: 'transparent',
      white: '#ffffff',
      black: '#000000',
      primary: {
        light: '#b3d1dd',
        DEFAULT: '#00648e',
        dark: '#00365f'
      },
      secondary: {
        light: '#f8d6bd',
        DEFAULT: '#e87722',
        dark: '#d8470c'
      },
      brown: {
        light: '#d7ccc8',
        DEFAULT: '#795548',
        dark: '#3e2723'
      },
      green: {
        light: '#dcedc8',
        DEFAULT: '#8bc34a',
        dark: '#33691e'
      },
      grey: {
        light: '#fbfbfb',
        DEFAULT: '#9e9e9e',
        dark: '#212121'
      },
      greyblue: {
        light: '#cfd8dc',
        DEFAULT: '#607d8b',
        dark: '#263238'
      },
      pink: {
        light: '#f8bbd0',
        DEFAULT: '#e81e62',
        dark: '#880e4f'
      },
      purple: {
        light: '#e1bee7',
        DEFAULT: '#9c27b0',
        dark: '#4a148c'
      },
      teal: {
        light: '#b2dfdb',
        DEFAULT: '#009688',
        dark: '#004d40'
      },
      yellow: {
        light: '#fff9c4',
        DEFAULT: '#ffeb3b',
        dark: '#f57f17'
      },
      red: {
        light: '#ffcdd2',
        DEFAULT: '#f44336',
        dark: '#ec1e16'
      },
      amber: {
        light: '#ffecb3',
        DEFAULT: '#ffc107',
        dark: '#ff6f00'
      }
    },
    extend: {
      fontFamily: {
        sans: ['"Open Sans"', ...defaultTheme.fontFamily.sans]
      },
      opacity: {
        87: '.87',
        54: '.54',
        38: '.38',
        12: '.12'
      }
    }
  },
  plugins: [
    plugin(({ addUtilities, theme }) => {
      const fontSizes = theme('fontSize');
      const letterSpacing = theme('letterSpacing');
      const fontWeight = theme('fontWeight');
      const fontFamily = theme('fontFamily');
      addUtilities({
        '.typo-4xl': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.normal,
          'font-size': fontSizes['2xl'][0],
          'line-height': fontSizes['2xl'][1],
          'letter-spacing': letterSpacing.wide
        },
        '.typo-3xl': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.bold,
          'font-size': fontSizes.xl[0],
          'line-height': fontSizes.xl[1],
          'letter-spacing': letterSpacing.normal
        },
        '.typo-2xl': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.normal,
          'font-size': fontSizes.lg[0],
          'line-height': fontSizes.lg[1],
          'letter-spacing': letterSpacing.normal
        },
        '.typo-xl': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.bold,
          'font-size': fontSizes.lg[0],
          'line-height': fontSizes.lg[1],
          'letter-spacing': letterSpacing.normal
        },
        '.typo-lg': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.normal,
          'font-size': fontSizes.base[0],
          'line-height': fontSizes.base[1],
          'letter-spacing': letterSpacing.normal
        },
        '.typo-base': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.bold,
          'font-size': fontSizes.base[0],
          'line-height': fontSizes.base[1],
          'letter-spacing': letterSpacing.tightest
        },
        '.typo-sm': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.normal,
          'font-size': fontSizes.sm[0],
          'line-height': fontSizes.sm[1],
          'letter-spacing': letterSpacing.tight
        },
        '.typo-xs': {
          'font-family': fontFamily.sans.join(),
          'font-weight': fontWeight.normal,
          'font-size': fontSizes.xs[0],
          'line-height': fontSizes.xs[1],
          'letter-spacing': letterSpacing.tight
        }
      });
    })
  ]
};
