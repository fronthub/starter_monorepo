/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.spec.ts$': 'ts-jest',
    '^.+\\.vue$': 'vue3-jest'
  },
  moduleNameMapper: {
    '\\.(jpg|ico|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$': '<rootDir>/../../config/fileMock.js',
    '\\.(css|less)$': '<rootDir>/../../config/fileMock.js',
    '^@/(.*)$': '<rootDir>/src/$1'
  },
  testMatch: ['**/*.spec.ts'],
  globals: {
    'ts-jest': {
      tsconfig: './tsconfig.json'
    }
  },
  moduleFileExtensions: ['json', 'js', 'jsx', 'ts', 'tsx', 'vue']
};
